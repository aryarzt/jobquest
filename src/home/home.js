import { Link } from "react-router-dom"


const Home = () => {
return (
<section className="bg-white dark:bg-gray-900 pt-16 lg:pt-0">
  <div className="grid max-w-screen-xl px-4 py-8 mx-auto gap-8 lg:grid-cols-12 lg:py-16">
    <div className="lg:col-span-7 lg:place-self-center">
      <h6 className="max-w-2xl mb-4 text-4xl font-extrabold tracking-tight leading-none md:text-5xl xl:text-6xl dark:text-white">Embark on Your JobQuest, Navigate Success!</h6>
      <p className="max-w-2xl mb-6 text-gray-500 lg:mb-8 md:text-lg lg:text-xl dark:text-gray-300">Your Career Quest Companion. Navigate the job market with ease. Find your dream job effortlessly!</p>
      <Link to="/roles" className="inline-flex items-center justify-center px-5 py-3 text-base font-medium text-center text-gray-500 border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 dark:text-white dark:border-gray-700 dark:hover:bg-gray-700 dark:focus:ring-gray-800">
        Begin Your Search For A Role
      </Link>
    </div>
    <div className="lg:col-span-5 lg:flex lg:justify-center">
      <img src="https://media.istockphoto.com/id/1420661831/vector/vector-illustration-material-of-a-professional-business-person.jpg?s=612x612&w=0&k=20&c=hP7QyI4fhaHyXWVVOFIMhN5yp6ZA17prNStMBgUsN8Y=" alt="jobquest" className="w-full md:max-w-md lg:max-w-full" />
    </div>
  </div>
    <div className="bg-white dark:bg-gray-900 py-8 px-4 mx-auto max-w-screen-xl lg:py-16">
      <div className="bg-gray-50 dark:bg-gray-800 border border-gray-200 dark:border-gray-700 rounded-lg p-8 md:p-12 mb-8">
        <h1 className="text-gray-900 dark:text-white text-3xl md:text-3xl font-extrabold mb-2">Welcome to the Adventure-Packed Career World! This is the list of the most popular roles.</h1>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">

            <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
              <img src="https://static.thenounproject.com/png/1266207-200.png" alt="Code" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Code Maker</h3>
                <p className="text-gray-600">Challenge yourself in developing innovative tech solutions.</p>
              </div>

            <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
             <img src="https://www.freeiconspng.com/uploads/money-bag-icon-png-23.png" alt="financial" className="w-8 h-8 object-cover"/>
             <h3 className="text-xl font-semibold mb-2">Financial Master</h3>
             <p className="text-gray-600">Explore the treasure trove of financial data to uncover stories behind the financial of your group.</p>
            </div>

            <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
                <img src="https://www.svgrepo.com/show/153669/teacher-at-the-blackboard.svg" alt="mentor" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Mentor</h3>
                <p className="text-gray-600">Providing guidance, support, and advice to students both academically and personally.</p>
              </div>

              <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
                <img src="https://png.pngtree.com/png-vector/20230218/ourmid/pngtree-viral-marketing-icon-png-image_6607442.png" alt="marketing" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Marketing Broadcaster</h3>
                <p className="text-gray-600">Conquer the digital realm to promote and deliver your product.</p>
              </div>

              <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
                <img src="https://cdn-icons-png.flaticon.com/512/450/450166.png" alt="chef" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Culinary Creativity</h3>
                <p className="text-gray-600">Create and design menus, invent recipes, and showcase their culinary creativity by preparing diverse and delicious dishes.</p>
              </div>

              <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
                <img src="https://cdn-icons-png.flaticon.com/512/1985/1985474.png" alt="network" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Network Expert</h3>
                <p className="text-gray-600">Surround and maintain the tech world to keep it stable.</p>
              </div>

              <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
                <img src="https://cdn-icons-png.flaticon.com/512/206/206906.png" alt="writer" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Legendary Writer</h3>
                <p className="text-gray-600">Write an unforgettable stories for various purposes.</p>
              </div>

              <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
                <img src="https://cdn-icons-png.flaticon.com/512/1196/1196918.png" alt="security" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Guardian</h3>
                <p className="text-gray-600">Defenders of headquarter to make a situation around the headquarter is safe</p>
              </div>

              <div className="border p-4 rounded-md shadow-md relative flex flex-col items-center">
                <img src="https://www.iconpacks.net/icons/1/free-doctor-icon-284-thumb.png" alt="doctor" className="w-8 h-8 object-cover"/>
                <h3 className="text-xl font-semibold mb-2">Healer</h3>
                <p className="text-gray-600">The doctor/healer is like a guardian angel. They specialize in fixing injuries and making sure everyone stays healthy.</p>
              </div>
            </div>
      </div>
    </div>
  <div className="px-4 mx-auto max-w-screen-xl">
    <div className="bg-gray-50 dark:bg-gray-800 border border-gray-200 dark:border-gray-700 rounded-lg p-8 md:p-12 mb-8">
        <h1 className="text-gray-900 dark:text-white text-3xl md:text-3xl font-extrabold mb-4">Explore Opportunities with These Partner Companies.</h1>
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 justify-items-center">
          <div className="flex justify-center items-center">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Bank_Mandiri_logo_2016.svg/2560px-Bank_Mandiri_logo_2016.svg.png" alt="Mandiri" className="h-16 w-auto" />
          </div>
          <div className="flex justify-center items-center">
            <img src="https://jarisnk.com/wp-content/uploads/2019/10/Logo-Tokopedia.png" alt="Tokopedia" className="h-16 w-auto" />
          </div>
          <div className="flex justify-center items-center">
            <img src="https://www.telkom.co.id/minio/show/data/image_upload/page/1594112773573_compress_PNG%20Logo%20Sekunder%20Telkom.png" alt="Telkom Indonesia" className="h-16 w-auto" />
          </div>
          <div className="flex justify-center items-center">
            <img src="https://www.pertamina.com/Media/Image/Pertamina.png" alt="Pertamina" className="h-16 w-auto" />
          </div>
          <div className="flex justify-center items-center">
            <img src="https://upload.wikimedia.org/wikipedia/id/thumb/8/8b/Cnn.svg/1200px-Cnn.svg.png" alt="CNN" className="h-16 w-auto" />
          </div>
          <div className="flex justify-center items-center">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Google_logo_%282013-2015%29.svg/1280px-Google_logo_%282013-2015%29.svg.png" alt="Google" className="h-16 w-auto" />
          </div>
          <div className="flex justify-center items-center">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Logo_PLN.svg/2560px-Logo_PLN.svg.png" alt="PLN" className="h-16 w-auto" />
          </div>
          <div className="flex justify-center items-center">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/ASTRA_international.svg/2560px-ASTRA_international.svg.png" alt="Astra" className="h-16 w-auto" />
          </div>
        </div>
    </div>
  </div>
</section>
)
}
export default Home