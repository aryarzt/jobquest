import React, { useState } from 'react'
import { useEffect } from 'react'
import axios from 'axios'
import { Table,Button } from 'flowbite-react'


const Vacancy = () => {
    const [data,setData] = useState(null)

    const getDataVacancy = () => {
      axios.get("https://dev-example.sanbercloud.com/api/job-vacancy").then((res) =>{
        setData([...res.data.data])
        console.log(res.data.data)
    }).catch((error) => {
      console.log(error)
    })
    }
    useEffect(() => {
      getDataVacancy()
    }, [])

    const [selectedData, setSelectedData] = useState(null);

    const handleCardClick = (res) => {
      setSelectedData(res);
    };

    return (
      <div className="mt-20 px-4 py-8 mx-auto lg:gap-8 xl:gap-0 lg:py-16 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4 grid-row-gap-8">
        {data !== null && data.map((res, index) => {
          return (
            <div className="w-86 h-86 mt-8 max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
              <img
              className="rounded-t-lg w-full h-48 object-cover"
              src={res.company_image_url ? res.company_image_url : 'https://e7.pngegg.com/pngimages/829/733/png-clipart-logo-brand-product-trademark-font-not-found-logo-brand.png'}
              alt=""/>
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{res.company_name}</h5>
                </a>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">{res.job_description }, <br></br>{res.job_qualification}, <br></br> {res.job_tenure}</p>
                <a href="#" onClick={() => handleCardClick(res)} className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                  Read more
                  <svg className="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M1 5h12m0 0L9 1m4 4L9 9" />
                  </svg>
                </a>
              </div>
            </div>
          );
        })}

      {selectedData && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <div className="max-w-md p-8 bg-white rounded-lg shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Guild Name: {selectedData.company_name} ({selectedData.job_type})</h2>
            <h2 className="text-2xl font-bold mb-4">Role: {selectedData.title}</h2>
            <p className="mb-4">Description: {selectedData.job_description}<br></br><br></br>
            Qualification: {selectedData.job_qualification}<br></br><br></br>
            Recruitment Status: {selectedData.job_status === 1 ? 'Open' : 'Closed'}<br></br><br></br>
            Location: {selectedData.company_city}<br></br><br></br>
            Monthly Reward(Salary): {selectedData.salary_min} - {selectedData.salary_max}
            </p>
            <button className='inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-red-700 rounded-lg hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-blue-800' onClick={() => setSelectedData(null)}>Close</button>
          </div>
        </div>
      )}
    </div>
  );
};
  
    export default Vacancy