import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Navbar from './components/navbar';
import { GlobalProvider } from './context/GlobalContext';
import Home from './home/home';
import Vacancy from './vacancy/vacancy';
import Footer from './components/footer';

function App() {
  return (
    <div className="App">

      <BrowserRouter>
      <Navbar />
      <GlobalProvider>
        <Routes>
        <Route path="/" element={<Home></Home>}/>
        <Route path="/roles" element={<Vacancy></Vacancy>} />
        </Routes>
      </GlobalProvider>
      <Footer />
      </BrowserRouter>
      
      {/* <Logo /> */}
    </div>
  );
}

export default App;
