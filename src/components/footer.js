import React from "react";

const Footer = () => {
  const currentYear = new Date().getFullYear();

  return (
    <footer className="bg-gradient-to-br from-blue-400 to-purple-600 py-4 text-center text-white">
      <p>&copy; {currentYear} Muhammad Arya Dhafa Rodial Widi | JobQuest</p>
    </footer>
  );
};

export default Footer;